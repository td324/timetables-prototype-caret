Django == 1.4
South == 0.7.6
psycopg2
pytz
# django-haystack == 1.2.7 # (not currently used)
unipath
icalendar
python-ldap
